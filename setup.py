#!/usr/bin/env python

from distutils.core import setup

setup(name='settings',
      version='0.1.0',
      description='Micropython package work with settings',
      author='Jan Klusáček',
      author_email='honza.klugmail.com',
      url='https://gitlab.com/users/honza.klu',
      packages=['settings'],
     )