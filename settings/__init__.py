import json

SETTINGS_PATH = "/settings.json"

sett_obj = None


def get_settings(req_settings=None):
    global sett_obj
    if sett_obj is None:
        sett_obj = Settings(req_settings)
    return sett_obj


class Settings:
    def __init__(self, req_settings=None):
        if req_settings is None:
            req_settings = {}
        self.req_settings = req_settings
        try:
            self.load()
        except OSError:
            self.settings = dict(self.req_settings)

    def load(self):
        with open(SETTINGS_PATH, "r") as f:
            self.settings = json.load(f)
        for k, v in self.req_settings.items():
            if k not in self.settings:
                self.settings[k] = v

    def save(self):
        with open(SETTINGS_PATH, "w") as f:
            json.dump(self.settings, f)